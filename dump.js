const r = require('rethinkdb');
const fs = require('fs');
const path = require('path');
const cliProgress = require('cli-progress');

class Main {

    async start() {
        let args = process.argv.splice(2);

        this.limit = 4000;
        this.host = args[0];
        this.port = args[1];
        this.db = args[2];
        this.path = path.join('backup', args[3]);

        if (!fs.existsSync('backup')) {
            fs.mkdirSync('backup');
        }

        fs.rmdirSync(this.path, {recursive: true})
        fs.mkdirSync(this.path);
        await this.connect();
        await this.dump();
        return 1;
    }

    async connect() {
        let conn = new Promise((resolve) => {
            r.connect({host: this.host, port: this.port}, (err, conn) => {
                if (err) {
                    throw err;
                }

                resolve(conn);
            });
        });

        this.connection = await conn;
    }

    async getTables() {
        return new Promise((resolve) => {
            r.db(this.db).tableList().run(this.connection, (err, result) => {
                if (err) throw err;
                resolve(result);
            });
        });
    }

    async getIndex(table) {
        let indexList = new Promise((resolve) => {
            r.db(this.db).table(table).indexList().run(this.connection, (err, result) => {
                if (err) throw err;
                resolve(result);
            });
        });
        let list = await indexList;
        let indexListExport = {};
        for (let i = 0; i < list.length; i++) {
            let index = new Promise((resolve) => {
                r.db(this.db).table(table).indexStatus(list[i])
                    .run(this.connection, (err, result) => {
                        if (err) throw err;
                        resolve(result[0].query);
                    });
            });
            indexListExport[list[i]] = await index;
        }
        fs.writeFileSync(`${this.path}/${table}.index.json`, JSON.stringify(indexListExport));
    }

    pad(str, padStr, len) {
        while (str.length < len)
            str = `${padStr}${str}`;
        return str;
    }

    async getData(table) {
        let format = (options, params, payload) => {
            const progress = this.pad(`${parseInt(params.progress * 100)}`, '0', 3);
            const completeSize = Math.round(params.progress * options.barsize);
            const incompleteSize = options.barsize - completeSize;
            const bar = options.barCompleteString.substr(0, completeSize) + options.barGlue +
                options.barIncompleteString.substr(0, incompleteSize);
            const value = this.pad(`${params.value}`, '0', 10);
            const total = this.pad(`${params.total}`, '0', 10);


            return `${bar} | ${progress}% | ${value}/${total} | ${table}`;
        };

        let bar = new cliProgress.SingleBar({
            format: format,
            barCompleteChar: '\u2588',
            barIncompleteChar: '\u2591',
            hideCursor: true
        });
        let count = new Promise((resolve) => {
            r.db(this.db).table(table).count().run(this.connection, (err, result) => {
                if (err) throw err;
                resolve(result);
            });
        });
        let max = await count;
        bar.start(max, 0);
        let limit = this.limit;
        let skip = 0
        let cont = 0;
        do {
            let data = new Promise((resolve) => {
                r.db(this.db).table(table).skip(skip).limit(limit).coerceTo('ARRAY').run(this.connection, (err, result) => {
                    if (err) {
                        throw err;
                    }
                    resolve(result);
                });
            });
            let itens = await data;
            if (itens.length > 0) {
                bar.increment(itens.length);
                try {
                    fs.writeFileSync(`${this.path}/${table}.${this.pad(`${cont}`, `0`, 5)}.data.json`, JSON.stringify(itens));
                } catch (e) {
                    console.log(itens, r.db(this.db).table(table).skip(skip).limit(limit).coerceTo('ARRAY').toString());
                }
            } else {
                fs.writeFileSync(`${this.path}/${table}.${this.pad(`${cont}`, `0`, 5)}.data.json`, JSON.stringify([]));
            }

            cont++;
            skip = skip + limit;
        } while (skip < max);
        bar.stop();
        // console.log(`Finalizado ${table}`);
    }

    dump() {
        return new Promise(async (resolve) => {

            let tables = await this.getTables();
            //console.log(tables);

            fs.writeFileSync(`${this.path}/create.table.json`, JSON.stringify(tables));

            for (let i = 0; i < tables.length; i++) {
                await this.getIndex(tables[i]);
                await this.getData(tables[i]);
            }

            resolve();
        })
    }

}

let main = new Main();
main.start().then(() => {
    process.exit();
});
