const r = require('rethinkdb');
const fs = require('fs');
const path = require('path');
const cliProgress = require('cli-progress');

class Main {

    async start() {
        let args = process.argv.splice(2);

        this.host = args[0];
        this.port = args[1];
        this.db = args[2];
        this.path = path.join('backup', args[3]);
        this.shards = parseInt(args[4]);
        this.replica = parseInt(args[4]);

        await this.connect();
        await this.restore();
        return true;
    }

    async connect() {
        let conn = new Promise((resolve) => {
            r.connect({host: this.host, port: this.port}, (err, conn) => {
                if (err) {
                    throw err;
                }

                resolve(conn);
            });
        });

        this.connection = await conn;
    }

    read(file) {
        let data = fs.readFileSync(file, {encoding: 'utf-8'}) || "[]";
        return JSON.parse(data);
    }

    listFiles() {
        return new Promise((resolve) => {
            fs.readdir(`${this.path}`, (err, files) => {
                resolve(files);
            })
        });
    }

    dropDb() {
        return new Promise((resolve) => {
            r.dbDrop(this.db).run(this.connection, () => {
                resolve();
            });
        });
    }

    createDb() {
        return new Promise((resolve) => {
            r.dbCreate(this.db).run(this.connection, () => {
                resolve();
            });

        });
    }

    createTable(table) {
        return new Promise((resolve) => {
            r.db(this.db).tableCreate(table).run(this.connection, () => {
                resolve();
            });
        });
    }

    reconfigure(table) {
        return new Promise((resolve) => {
            if (this.shards > 1 || this.replica > 1) {
                r.db(this.db).table(table)
                    .reconfigure({
                        shards: this.shards,
                        replicas: this.replica
                    })
                    .run(this.connection, () => {
                        resolve();
                    });
            } else {
                resolve();
            }
        });
    }

    createIndex(table, create) {
        return new Promise((resolve) => {
            let t = `r.db('${this.db}').table('${table}').${create}`;
            let e = eval(t);

            if (create.indexOf('r.row') !== -1) {
                let replaceFunction = t.replace(/function[^(]*\(([^)]*)\)/, 'function(row)');
                let replaceRow = replaceFunction.replace(/r.row/g, 'row');
                e = eval(replaceRow);
            }

            e.run(this.connection, () => {
                resolve();
            });
        });
    }

    setData(file, table) {
        return new Promise(async (resolve) => {
            let data = this.read(`${this.path}/${file}`);
            r.db(this.db).table(table).insert(data).run(this.connection, (err) => {
                if(err) {
                    console.log(err, file);
                }
                resolve();
            });
        })
    }

    pad(str, padStr, len) {
        while (str.length < len)
            str = `${padStr}${str}`;
        return str;
    }

    async restore() {
        await this.dropDb();
        await this.createDb();
        let listFiles = await this.listFiles();

        let format = (options, params, payload) => {
            const progress = this.pad(`${parseInt(params.progress * 100)}`, '0', 3);
            const completeSize = Math.round(params.progress * options.barsize);
            const incompleteSize = options.barsize - completeSize;
            const bar = options.barCompleteString.substr(0, completeSize) + options.barGlue +
                options.barIncompleteString.substr(0, incompleteSize);
            const value = this.pad(`${params.value}`, '0', 10);
            const total = this.pad(`${params.total}`, '0', 10);
            return `${bar} | ${progress}% | ${value}/${total}`;
        };

        let bar = new cliProgress.SingleBar({
            format: format,
            barCompleteChar: '\u2588',
            barIncompleteChar: '\u2591',
            hideCursor: true
        });
        bar.start(listFiles.length, 0);

        let tables = this.read(`${this.path}/create.table.json`);
        bar.increment();

        for (let i = 0; i < tables.length; i++) {
            await this.createTable(tables[i]);
            try {
                for (let y = 0; y < listFiles.length; y++) {
                    if (listFiles[y].indexOf(`${tables[i]}.`) !== -1 &&
                        listFiles[y].indexOf('index') === -1) {
                        await this.setData(listFiles[y], tables[i]);
                        bar.increment();
                    }
                }
            } catch (e) {
                console.log(e);
            }
            await this.reconfigure(tables[i]);
        }

        for (let i = 0; i < tables.length; i++) {
            try {
                let indexCreate = this.read(`${this.path}/${tables[i]}.index.json`);
                bar.increment();
                let keys = Object.keys(indexCreate);
                for (let y = 0; y < keys.length; y++) {
                    await this.createIndex(tables[i], indexCreate[keys[y]]);
                }
            } catch (e) {
                console.log(e);
            }
        }

        bar.stop();
        return true;
    }

}

let main = new Main();
main.start().then(() => {
    process.exit();
});
