### Comando Dump

```bash
node dump.js ip 28015 db ./path
```

### Comando Restore

```bash
node restore.js ip 28015 db ./path replicas
```
